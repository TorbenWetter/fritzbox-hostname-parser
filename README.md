## Prerequisites
- Create a .env file containing your FRITZBOX_URL, FRITZBOX_USER and FRITZBOX_PASSWORD
- Copy the current content of your /etc/hosts file into the project's hosts file (the FritzBox devices are then appended)
- Make sure to have enough rights to edit your /etc/hosts file. It is not the best idea to run this script as root though

## Installing the npm packages
```
npm install
```

## Executing the script
```
npm start
```