const fs = require('fs');
require('dotenv/config');
const fritz = require('fritzapi');
const request = require('request');

let hosts = fs.readFileSync('hosts') + '\n';

fritz
  .getSessionID(process.env.FRITZBOX_USER, process.env.FRITZBOX_PASSWORD, {
    url: process.env.FRITZBOX_URL,
  })
  .then((sid) => {
    request.post(
      {
        url: process.env.FRITZBOX_URL + '/data.lua',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        form: { sid, page: 'netDev' },
      },
      (err, res, body) => {
        if (err || body.trim() === '') {
          console.error(err || 'Empty response');
          return;
        }

        const data = JSON.parse(body).data;
        const devices = data.active.concat(data.passive);
        for (let device of devices) {
          hosts += device.ipv4.ip + ' ' + device.name.replace(' ', '-') + '\n';
        }
        fs.writeFileSync('/etc/hosts', hosts);
      }
    );
  })
  .catch((err) => console.error(err));
